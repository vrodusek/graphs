﻿using Graphs.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Collections.ObjectModel;
using Newtonsoft.Json.Linq;
using System.Data;
using System.ComponentModel;
using MaterialDesignThemes.Wpf;
using System.Windows.Controls.Primitives;

namespace Graphs
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public ObservableCollection<City> Cities { get; set; }
        public ObservableCollection<string> Criteriums { get; set; }
        public ObservableCollection<Weather> SelectedCityForecast { get; set; }
        public City SelectedCity { get; set; }
        private City PreviousCity { get; set; }
        private bool IsDeleting { get; set; }

        public MainWindow()
        {
            InitializeComponent();
            SetActive(true);
            Cities = new ObservableCollection<City>();
            Criteriums = new ObservableCollection<string>();
            SelectedCityForecast = new ObservableCollection<Weather>();
            PreviousCity = null;
            DataContext = this;
            radBtnDaily.IsChecked = true;
            slider.Minimum = 1;
            slider.Value = 3;
            currentWeather.CanUserReorderColumns = false;
            IsDeleting = false;
        }

        private void SetActive(bool ind)
        {
            cityTextBox.IsEnabled = ind;
            showButton.IsEnabled = ind;
        }

        private static City ProcessRepositories(string apiCall)
        {
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(apiCall);
            Stream s = request.GetResponse().GetResponseStream();
            var serializer = new JsonSerializer();
            using (var sr = new StreamReader(s))
            using (var jsonTextReader = new JsonTextReader(sr))
            {
                return serializer.Deserialize<City>(jsonTextReader);
            }
        }

        private bool ContainsCity(string cityName)
        {
            foreach(City city in Cities)
            {
                if (city.CityName.ToLower() == cityName.ToLower())
                    return true;
            }

            return false;
        }
        

        private async void AddCity()
        {
            string name = cityTextBox.Text;
            cityTextBox.Text = "";
            Dictionary<string, object> queryParams = new Dictionary<string, object>();
            var city = await getCityFromJson(name);

            SetActive(true);

            if (city != null && !ContainsCity(name))
            {
                var lat = (double)city["coord"]["lat"];
                var lng = (double)city["coord"]["lon"];
                queryParams["lat"] = lat;
                queryParams["lon"] = lng;
                queryParams["units"] = "metric";
                queryParams["appid"] = apiKey.API_KEY;

                string apiCall = generateAPICall(queryParams);
                City c = ProcessRepositories(apiCall);
                c.CityName = city["name"].ToString();
                Cities.Add(c);

                FIELD field = City.GetEnumFromString(comboBoxCriteria.SelectedItem?.ToString());
                if (radBtnDaily.IsChecked == true)
                {
                    int numDays = (int)slider.Value;
                    forecast.AddCityWeather(c.GetDaily(field, numDays));
                    forecast.SetLabels(c.GetDateRange(numDays));
                }
                else
                {
                    int numHours = (int)slider.Value;
                    forecast.AddCityWeather(c.GetHourly(field, numHours));
                    forecast.SetLabels(c.GetHourRange(numHours));
                }
            }
            else if(city == null)
                MessageBox.Show("We can not find that city in our system.\nPlease type again", 
                    "Info", MessageBoxButton.OK, MessageBoxImage.Information);
            else
                MessageBox.Show("This city is already added to forecast.",
                    "Info", MessageBoxButton.OK, MessageBoxImage.Information);
        }

        private void ShowButton_Click(object sender, RoutedEventArgs e)
        {
            SetActive(false);
            AddCity();
        }
        
        private async Task<JObject> getCityFromJson(string cityName)
        {
            JsonSerializer serializer = new JsonSerializer();
            using (FileStream s = File.Open("Data/city.list.json", FileMode.Open))
            using (StreamReader sr = new StreamReader(s))
            using (JsonReader reader = new JsonTextReader(sr))
            {
                while(true)
                {
                    bool ind = await reader.ReadAsync();
                    if (!ind)
                        break;

                    if (reader.TokenType == JsonToken.StartObject)
                    {
                        var token = serializer.Deserialize<JObject>(reader);
                        if (token["name"].ToString().ToLower() == cityName.ToLower())
                            return token;
                    }
                }
            }
            return null;
        }

        private string generateAPICall(Dictionary<string, object> queryParams)
        {
            string linkBase = "http://api.openweathermap.org/data/2.5/onecall";

            List<string> keyValue = new List<string>();
            foreach(var param in queryParams)
            {
                keyValue.Add($"{param.Key}={param.Value.ToString()}");
            }

            return $"{linkBase}?{string.Join("&", keyValue)}";
        }
        
        private void RadBtnDaily_Checked(object sender, RoutedEventArgs e)
        {
            slider.Maximum = 7;
            slider.Value = 3;
            forecast.ClearSeries();
            FIELD field = City.GetEnumFromString(comboBoxCriteria.SelectedItem?.ToString());
            foreach (City city in Cities)
            {
                forecast.AddCityWeather(city.GetDaily(field, 3));
            }
            if(Cities.Count != 0)
                forecast.SetLabels(Cities[0].GetDateRange(3));

            Criteriums.Clear();
            foreach (string criterium in City.CRITERIUMS_DAY)
                Criteriums.Add(criterium);

            comboBoxCriteria.SelectedIndex = 0;
            if (SelectedCity != null)
                PlaceData();
        }

        private void RadBtnHourly_Checked(object sender, RoutedEventArgs e)
        {
            slider.Maximum = 48;
            slider.Value = 12;
            forecast.ClearSeries();
            FIELD field = City.GetEnumFromString(comboBoxCriteria.SelectedItem?.ToString());
            foreach (City city in Cities)
            {
                forecast.AddCityWeather(city.GetHourly(field, 12));
            }
            if (Cities.Count != 0)
                forecast.SetLabels(Cities[0].GetHourRange(12));

            Criteriums.Clear();
            foreach (string criterium in City.CRITERIUMS_HOUR)
                Criteriums.Add(criterium);

            comboBoxCriteria.SelectedIndex = 0;
            if (SelectedCity != null)
                PlaceData();
        }

        private void Slider_ValueChanged(object sender, RoutedPropertyChangedEventArgs<double> e)
        {
            int value = (int)slider.Value;
            intervalLabel.Content = value.ToString();
            FIELD field = City.GetEnumFromString(comboBoxCriteria.SelectedItem?.ToString());
            forecast.ClearSeries();
            foreach (City city in Cities)
            {
                if(radBtnDaily.IsChecked == true)
                    forecast.AddCityWeather(city.GetDaily(field, value));
                else
                    forecast.AddCityWeather(city.GetHourly(field, value));
            }
            if (Cities.Count != 0)
                if (radBtnDaily.IsChecked == true)
                    forecast.SetLabels(Cities[0].GetDateRange(value));
                else
                    forecast.SetLabels(Cities[0].GetHourRange(value));

            if(SelectedCity != null)
                PlaceData();
        }

        private void ComboBoxCriteria_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int value = (int)slider.Value;
            FIELD field = City.GetEnumFromString(comboBoxCriteria.SelectedItem?.ToString());
            forecast.ClearSeries();
            foreach (City city in Cities)
            {
                if (radBtnDaily.IsChecked == true)
                    forecast.AddCityWeather(city.GetDaily(field, value));
                else
                    forecast.AddCityWeather(city.GetHourly(field, value));
            }
            switch(field)
            {
                case FIELD.TEMP:
                case FIELD.MAX:
                case FIELD.MIN:
                    forecast.SetFormatter("°C");
                    break;
                case FIELD.HUMIDITY:
                    forecast.SetFormatter(" %");
                    break;
                case FIELD.PRESSURE:
                    forecast.SetFormatter(" hpa");
                    break;
                case FIELD.WIND_SPEED:
                    forecast.SetFormatter(" m/s");
                    break;
                default:
                    forecast.SetFormatter("");
                    break;
            }

            string title = "Temperature";
            if(comboBoxCriteria.SelectedItem != null)
            {
                title = comboBoxCriteria.SelectedItem.ToString();
            }
            forecast.YAxisTitle = title;
        }

        private int GetCityIndex(string cityName)
        {
            for(int i = 0; i < Cities.Count; i++)
            {
                if (Cities[i].CityName == cityName)
                    return i;
            }

            return -1;
        }

        private void CurrentWeather_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (!IsDeleting)
            {
                if (PreviousCity == null)
                    PreviousCity = SelectedCity = currentWeather.SelectedItem as City;
                else
                {
                    PreviousCity = SelectedCity;
                    SelectedCity = currentWeather.SelectedItem as City;
                    if (SelectedCity == null)
                        SelectedCity = PreviousCity;
                }

                PlaceData();
                futureForecast.Visibility = Visibility.Visible;
                selCityText.Visibility = Visibility.Visible;
                selCityText.Text = SelectedCity.CityName;
            }
        }

        private void PlaceData()
        {
            ICollectionView view = CollectionViewSource.GetDefaultView(futureForecast.ItemsSource);
            if (view != null)
            {
                view.SortDescriptions.Clear();
                foreach (DataGridColumn column in futureForecast.Columns)
                {
                    column.SortDirection = null;
                }
            }
            SelectedCityForecast.Clear();
            int value = (int)slider.Value;
            if (radBtnDaily.IsChecked == true)
            {
                if (value > 7)
                    value = 7;
                for (int i = 0; i <= value; i++)
                    SelectedCityForecast.Add(SelectedCity.Daily[i]);
            }
            else
            {
                if (value > 47)
                    value = 47;
                for (int i = 0; i <= value; i++)
                    SelectedCityForecast.Add(SelectedCity.Hourly[i]);
            }
        }

        private void CityTextBox_KeyUp(object sender, KeyEventArgs e)
        {
            if(e.Key == Key.Enter)
            {
                SetActive(false);
                AddCity();
                Focus();
            }
        }

        private void DataGridColumnSize()
        {
            if (ActualWidth > 1000)
            {
                foreach (var column in futureForecast.Columns)
                    column.Width = new DataGridLength(1, DataGridLengthUnitType.Star);
            }
            else
            {
                foreach (var column in futureForecast.Columns)
                    column.Width = new DataGridLength(0, DataGridLengthUnitType.Auto);
            }
        }

        private void Window_SizeChanged(object sender, SizeChangedEventArgs e)
        {
            DataGridColumnSize();
        }

        private void Window_StateChanged(object sender, EventArgs e)
        {
            DataGridColumnSize();
        }

        private void DeleteCity()
        {
            SelectedCity = (City)currentWeather.SelectedItem;
            Cities.Remove(SelectedCity);

            int value = (int)slider.Value;
            FIELD field = City.GetEnumFromString(comboBoxCriteria.SelectedItem?.ToString());
            forecast.ClearSeries();
            foreach (City city in Cities)
            {
                if (radBtnDaily.IsChecked == true)
                    forecast.AddCityWeather(city.GetDaily(field, value));
                else
                    forecast.AddCityWeather(city.GetHourly(field, value));
            }
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            IsDeleting = true;
            DeleteCity();     
            IsDeleting = false;
            if (Cities.Count == 0)
            {
                SelectedCity = PreviousCity = null;
                SelectedCityForecast.Clear();
                selCityText.Visibility = Visibility.Hidden;
                futureForecast.Visibility = Visibility.Hidden;
            }
            else
            {
                if(PreviousCity != SelectedCity)
                    currentWeather.SelectedItem = PreviousCity;
                else
                {
                    SelectedCityForecast.Clear();
                    selCityText.Visibility = Visibility.Hidden;
                    futureForecast.Visibility = Visibility.Hidden;
                }
            }
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            Dictionary<string, object> queryParams = new Dictionary<string, object>();
            queryParams["lat"] = SelectedCity.Latitude;
            queryParams["lon"] = SelectedCity.Longitutde;
            queryParams["units"] = "metric";
            queryParams["appid"] = apiKey.API_KEY;

            string apiCall = generateAPICall(queryParams);
            City c = ProcessRepositories(apiCall);
            c.CityName = SelectedCity.CityName;
            string criteria = comboBoxCriteria.SelectedItem?.ToString();
            int value = (int)slider.Value;
            for (int i = 0; i < Cities.Count; i++)
            {
                if(c.CityName == Cities[i].CityName)
                {
                    SelectedCity = c;
                    Cities[i] = c;
                    if (radBtnDaily.IsChecked == true)
                        forecast.ChangeCityData(c.GetDaily(City.GetEnumFromString(criteria), value));
                    else
                        forecast.ChangeCityData(c.GetHourly(City.GetEnumFromString(criteria), value));
                    break;
                }
            }
        }
    }
}

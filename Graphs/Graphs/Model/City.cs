﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Graphs.Model
{
    public enum FIELD { TEMP, MAX, MIN, HUMIDITY, WIND_SPEED, PRESSURE };

    [JsonObject]
    public class City
    {
        public static readonly string[] CRITERIUMS_HOUR = {
            "Temperature", "Humidity", "Wind speed", "Pressure"
        };
        public static readonly string[] CRITERIUMS_DAY = {
            "Temperature", "Max temperature", "Min temperature", "Humidity", "Wind speed", "Pressure"
        };

        public string CityName { get; set; }

        [JsonProperty("lat")]
        public double Latitude { get; set; }

        [JsonProperty("lon")]
        public double Longitutde { get; set; }

        [JsonProperty("timezone")]
        public string Timezone { get; set; }

        [JsonProperty("current")]
        public CurrentWeather Current { get; set; }

        [JsonProperty("hourly")]
        public ObservableCollection<HourWeather> Hourly { get; set; }

        [JsonProperty("daily")]
        public ObservableCollection<DayWeather> Daily { get; set; }

        public void CityCopy(City copy)
        {
            Current = copy.Current;
            Hourly.Clear();
            Daily.Clear();

            foreach (var weather in copy.Daily)
                Daily.Add(weather);

            foreach (var weather in copy.Hourly)
                Hourly.Add(weather);
        }

        public string[] GetDateRange(int endDay)
        {
            if (endDay > 7)
                endDay = 7;
            var dates = new List<string>();
            for(int i = 0; i <= endDay; i++)
            {
                dates.Add(Daily[i].CurrentDate.ToShortDateString());
            }

            return dates.ToArray();
        }

        public string[] GetHourRange(int endHour)
        {
            if (endHour > 47)
                endHour = 47;
            var dates = new List<string>();
            for (int i = 0; i <= endHour; i++)
            {
                dates.Add(Hourly[i].CurrentDate.ToShortTimeString());
            }

            return dates.ToArray();
        }

        public static FIELD GetEnumFromString(string strValue)
        {
            switch (strValue)
            {
                case "Temperature":
                    return FIELD.TEMP;
                case "Max temperature":
                    return FIELD.MAX;
                case "Min temperature":
                    return FIELD.MIN;
                case "Humidity":
                    return FIELD.HUMIDITY;
                case "Wind speed":
                    return FIELD.WIND_SPEED;
                case "Pressure":
                    return FIELD.PRESSURE;
                default:
                    return FIELD.TEMP;
            }
        }

        public KeyValuePair<string, List<double>> GetDaily(FIELD fieldType, int length)
        {
            if (length > 7)
                length = 7;
            var results = new List<double>();
            switch(fieldType)
            {
                case FIELD.TEMP:
                    for(int i = 0; i <= length; i++)
                    {
                        results.Add(Daily[i].Temperature.Day);
                    }
                    break;
                case FIELD.MAX:
                    for (int i = 0; i <= length; i++)
                    {
                        results.Add(Daily[i].Temperature.Max);
                    }
                    break;
                case FIELD.MIN:
                    for (int i = 0; i <= length; i++)
                    {
                        results.Add(Daily[i].Temperature.Min);
                    }
                    break;
                case FIELD.HUMIDITY:
                    for (int i = 0; i <= length; i++)
                    {
                        results.Add(Daily[i].Humidity);
                    }
                    break;
                case FIELD.WIND_SPEED:
                    for (int i = 0; i <= length; i++)
                    {
                        results.Add(Daily[i].WindSpeed);
                    }
                    break;
                case FIELD.PRESSURE:
                    for (int i = 0; i <= length; i++)
                    {
                        results.Add(Daily[i].Pressure);
                    }
                    break;
                default:
                    break;
            }

            return new KeyValuePair<string, List<double>>(CityName, results);
        }

        public KeyValuePair<string, List<double>> GetHourly(FIELD fieldType, int length)
        {
            if (length > 47)
                length = 47;
            var results = new List<double>();
            switch (fieldType)
            {
                case FIELD.TEMP:
                    for (int i = 0; i <= length; i++)
                    {
                        results.Add(Hourly[i].Temperature);
                    }
                    break;
                case FIELD.HUMIDITY:
                    for (int i = 0; i <= length; i++)
                    {
                        results.Add(Hourly[i].Humidity);
                    }
                    break;
                case FIELD.WIND_SPEED:
                    for (int i = 0; i <= length; i++)
                    {
                        results.Add(Hourly[i].WindSpeed);
                    }
                    break;
                case FIELD.PRESSURE:
                    for (int i = 0; i <= length; i++)
                    {
                        results.Add(Hourly[i].Pressure);
                    }
                    break;
                default:
                    break;
            }

            return new KeyValuePair<string, List<double>>(CityName, results);
        }
    }
}

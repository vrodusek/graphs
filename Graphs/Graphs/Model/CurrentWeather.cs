﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Graphs.Model
{
    [JsonObject]
    public class CurrentWeather
    {
        public DateTime CurrentDate { get; set; }

        [JsonProperty("temp")]
        public double CurrentTemperature { get; set; }

        [JsonProperty("pressure")]
        public int Pressure { get; set; }

        [JsonProperty("visibility")]
        public int Visibility { get; set; }

        [JsonProperty("humidity")]
        public int Humidity { get; set; }

        [JsonProperty("dt")]
        public double PlaceCurrentDate
        {
            set
            {
                CurrentDate = new DateTime(1970, 1, 1, 0, 0, 0, 0, System.DateTimeKind.Utc);
                CurrentDate = CurrentDate.AddSeconds(value).ToLocalTime();
            }
        }

        public void CopyCurrent(CurrentWeather copy)
        {
            CurrentDate = copy.CurrentDate;
            CurrentTemperature = copy.CurrentTemperature;
            Humidity = copy.Humidity;
            Pressure = copy.Pressure;
            Visibility = copy.Visibility;
        }
    }
}

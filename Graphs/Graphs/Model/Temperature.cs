﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Graphs.Model
{
    [JsonObject]
    public class Temperature : IComparable
    {
        [JsonProperty("day")]
        public double Day { get; set; }

        [JsonProperty("min")]
        public double Min { get; set; }

        [JsonProperty("max")]
        public double Max { get; set; }

        override
        public string ToString()
        {
            return $"Min {Min}°C\nAvg {Day}°C\nMax {Max}";
        }

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;

            Temperature otherTemperature = obj as Temperature;
            if (otherTemperature != null)
                return this.Day.CompareTo(otherTemperature.Day);
            else
                throw new ArgumentException("Object is not a Temperature");
        }
    }
}

﻿using Graphs.Model;
using LiveCharts;
using LiveCharts.Wpf;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Graphs.UserControls
{
    /// <summary>
    /// Interaction logic for Chart.xaml
    /// </summary>
    public partial class Chart : UserControl
    {
        public SeriesCollection SeriesCollection { get; set; }
        public ObservableCollection<string> Labels { get; set; }
        public Func<double, string> YFormatter { get; set; }

        public void ClearSeries()
        {
            SeriesCollection.Clear();
        }

        public void AddCityWeather(KeyValuePair<string, List<double>> cityForecast)
        {
            SeriesCollection.Add(new LineSeries
            {
                Title = cityForecast.Key,
                Values = new ChartValues<double>(cityForecast.Value)
            });
        }

        public bool ChangeCityData(KeyValuePair<string, List<double>> cityForecast)
        {
            for (int i = 0; i < SeriesCollection.Count; i++)
            {
                if (SeriesCollection[i].Title == cityForecast.Key)
                {
                    SeriesCollection[i].Values.Clear();
                    foreach (var val in cityForecast.Value)
                        SeriesCollection[i].Values.Add(val);

                    return true;
                }
            }
            return false;
        }

        public void SetLabels(string[] labels)
        {
            Labels.Clear();
            foreach(string label in labels)
                Labels.Add(label);
        }

        public string YAxisTitle
        {
            get
            {
                if (chart.AxisY.Count != 0)
                    return chart.AxisY[0].Title;
                else
                    return "";
            }
            set
            {
                if (chart.AxisY.Count != 0)
                    chart.AxisY[0].Title = value;
            }
        }

        public string XAxisTitle
        {
            get
            {
                if (chart.AxisX.Count != 0)
                    return chart.AxisX[0].Title;
                else
                    return "";
            }
            set
            {
                if (chart.AxisX.Count != 0)
                    chart.AxisX[0].Title = value;
            }
        }

        public void SetFormatter(string format)
        {
            YFormatter = value => value.ToString() + format;
            chart.AxisY[0].LabelFormatter = YFormatter;
        }

        public Chart()
        {
            InitializeComponent();
            SeriesCollection = new SeriesCollection();
            Labels = new ObservableCollection<string>();
            YFormatter = value => value.ToString() + "°C";

            DataContext = this;
        }
    }
}
